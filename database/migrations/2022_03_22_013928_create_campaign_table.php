<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campaigns', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id');
        $table->integer('user_id')->unsigned();
        $table->integer('msstatus_id')->unsigned();
        $table->integer('msresult_id')->unsigned();
        $table->string('campaign_name');
        $table->integer('qty');
        $table->text('notes')->nullable();            
        $table->timestamps();
      });

      Schema::table('campaigns', function($table) {
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('msstatus_id')->references('id')->on('msstatuses');
          $table->foreign('msresult_id')->references('id')->on('msresults');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}