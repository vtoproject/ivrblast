<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignResultTable extends Migration
{
    /**
     * Run the migrations.
     *
		 * 
     * @return void
     */
    public function up()
    {
			Schema::create('campaignresults', function (Blueprint $table) {
				$table->increments('id');    
				$table->integer('campaign_id')->unsigned();
				$table->dateTime('date_started');
				$table->dateTime('date_finished');
				$table->integer('total_call');
				$table->integer('success_call');
				$table->integer('failed_call');
				$table->integer('campaign_progress');       
				$table->timestamps();
		  });

      Schema::table('campaignresults', function($table) {
        $table->foreign('campaign_id')->references('id')->on('campaigns');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('campaignresult');
    }
}
