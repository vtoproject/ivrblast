<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('numbers', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('campaign_id')->unsigned();
				$table->string('account_id')->nullable();
				$table->string('name');
				$table->string('phone');
				$table->string('action')->nullable();
				$table->string('response')->nullable();
				$table->integer('nominal')->nullable();
				$table->dateTime('bill_date')->nullable();
				$table->dateTime('due_date')->nullable();
				$table->dateTime('call_dial')->nullable();
				$table->dateTime('call_connect')->nullable();
				$table->dateTime('call_disconnect')->nullable();
				$table->time('call_duration')->nullable();
				$table->timestamps();
			});

			Schema::table('numbers', function($table) {
				$table->foreign('campaign_id')->references('id')->on('campaigns');
			});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
