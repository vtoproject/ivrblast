<?php
use Faker\Generator as Faker;
use Illuminate\Support\Str;
 
$factory->define(App\Campaign::class, function (Faker $faker) {
    return [
      'campaign_name' => $this->faker->unique()->sentence(1),
      'qty' => mt_rand(10,100),
      'user_id' => mt_rand(1,5),
      'notes' => $this->faker->paragraph(),
      'ms_status_id' => mt_rand(1,4),
      'ms_result_id' => mt_rand(1,6),
      'campaign_result_id' => 0,
      'status_id' => mt_rand(1,2)
    ];
});

?>