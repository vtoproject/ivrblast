<?php

use App\User;
use App\Campaign;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
				// Campaign::Factory(10)->create();

				DB::table('users')->insert([
					'name' => 'Super user',
					'username' => 'superuser',
					'email' => 'superuser@dalnetsystem.com',
					'password' => bcrypt('password'),
					'role' => 'superuser',
					'status' => 'active'
				]);
				
				DB::table('users')->insert([
					'name' => 'Admin',
					'username' => 'admin',
					'email' => 'admin@dalnetsystem.com',
					'password' => bcrypt('password'),
					'role' => 'admin',
					'status' => 'active'
				]);	

				DB::table('users')->insert([
					'name' => 'Supervisor',
					'username' => 'supervisor',
					'email' => 'supervisor@dalnetsystem.com',
					'password' => bcrypt('password'),
					'role' => 'supervisor',
					'status' => 'active'
				]);		
				
				DB::table('msstatuses')->insert([
					'name' => 'Ready',
					'notes' => 'Campaign Ready'
				]);

				DB::table('msstatuses')->insert([
					'name' => 'Running',
					'notes' => 'Campaign Running'
				]);

				DB::table('msstatuses')->insert([
					'name' => 'Pause',
					'notes' => 'Campaign Pause'
				]);				

				DB::table('msstatuses')->insert([
					'name' => 'Finished',
					'notes' => 'Campaign Finish'
				]);

				DB::table('msresults')->insert([
					'name' => 'Date Started',
					'notes' => 'Call date started'
				]);

				DB::table('msresults')->insert([
					'name' => 'Date Finished',
					'notes' => 'Call date finish'
				]);				

				DB::table('msresults')->insert([
					'name' => 'Total Call',
					'notes' => 'Total call number'
				]);				
				
				DB::table('msresults')->insert([
					'name' => 'Success call',
					'notes' => 'Success call'
				]);				

				DB::table('msresults')->insert([
					'name' => 'Failed call',
					'notes' => 'Failed call number'
				]);

				DB::table('msresults')->insert([
					'name' => 'Campaign progress %',
					'notes' => 'Progress campaign call'
				]);			
				
				DB::table('msresponses')->insert([
					'name' => 'Answered',
					'notes' => 'Answered Number'
				]);

				DB::table('msresponses')->insert([
					'name' => 'No Answer',
					'notes' => 'No Answered Number'
				]);
				
				DB::table('msresponses')->insert([
					'name' => 'Busy',
					'notes' => 'Busy Number'
				]);
				
				DB::table('msresponses')->insert([
					'name' => 'Failed',
					'notes' => 'Failed Number'
				]);					
    }
}
