<?php

use App\Http\Controllers\CampaignController;

// Route::get('/', function () {
//     return view('auth.login');
// });

// Route::get('/test', [
// 	'uses' => 'TestController@index'
// 	]);

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', [
	'uses' => 'HomeController@getDashboard',
	'as' => 'dashboard',
	'middleware' => 'auth'
	]);

// Dashboard section
Route::get('/home', [
	'uses' => 'HomeController@getDashboard',
	'as' => 'dashboard',
	'middleware' => 'auth'
	]);

Route::get('/dashboard', [
	'uses' => 'HomeController@getDashboard',
	'as' => 'dashboard',
	'middleware' => 'auth'
	]);

// Route::get('/campaign/add',[
// 	'uses' => 'CampaignController@add',
// 	'as' => 'campaign',
// 	'middleware' => 'auth'
// ]);

// Route::post('/campaign/create',[
// 	'uses' => 'CampaignController@create',
// 	'as' => 'campaign',
// 	'middleware' => 'auth'	
// ]);

Route::get('/campaign/archive',[
	'uses' => 'CampaignController@archive',
	'as' => 'campaignarchive',
	'middleware' => 'auth'
]);

Route::resource('campaign','CampaignController');

Route::resource('users','UserController');

// Route::get('/users',[
// 	'uses' => 'UserController@index',
// 	'as' => 'campaign',
// 	'middleware' => 'auth'
// ]);	

// Route::get('/profile',[
// 	'uses' => 'UserController@profile',
// 	'as' => 'profile',
// 	'middleware' => 'auth'
// ]);

Route::get('/report', function () {
    return view('dashboard.report.commingsoon');
});