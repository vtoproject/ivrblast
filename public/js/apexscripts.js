var optionshalfdonut = {
  labels: ["Answer", "No Answer", "Busy", "Failed"],
  series: [50,30,10,10],
  chart: {
  type: 'donut',
},
plotOptions: {
  pie: {
    startAngle: -90,
    endAngle: 90,
    offsetY: 10
  }
},
grid: {
  padding: {
    bottom: -80
  }
},
responsive: [{
  breakpoint: 480,
  options: {
    chart: {
      width: 350
    },
    legend: {
      position: 'bottom'
    }
  }
}]
};

var charthalfdonut = new ApexCharts(document.querySelector("#halfdonut"), optionshalfdonut);
charthalfdonut.render();  

$(document).ready(function() {
var table = $('#datatbl').DataTable( {
    rowReorder: {
        selector: 'td:nth-child(2)'
    },
    responsive: true
} );
} );