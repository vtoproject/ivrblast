const pauseIconClassName = 'fa-pause-circle'
const playIconClassName = 'fa-play-circle'

const btns = document.querySelectorAll('.btn')

function onChange (event) {
  // get the button elememt from the event
    const buttonElement = event.currentTarget
  
  // check if play button class is present on our button
  const isPlayButton = buttonElement.classList.contains(playIconClassName)
  
  // if a play button, remove the play button class and add pause button class
  if (isPlayButton) {
    buttonElement.classList.remove(playIconClassName)
    buttonElement.classList.add(pauseIconClassName)
    // window.location.href = "runCampaign";
    
    // if a pause button, remove pause button class and add play button class
  } else {
    buttonElement.classList.remove(pauseIconClassName)
    buttonElement.classList.add(playIconClassName)
  }
}

// query selector all returns a list of nodes, therefore we need to iterate over it and attach an event listener to each button seperatly
btns.forEach(btn => {
    btn.addEventListener('click', onChange)
})