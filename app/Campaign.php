<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    //
    protected $fillable = [
        'user_id', 'campaign_name', 'msresult_id', 'msstatus_id', 'msresponse_id', 'qty', 'notes',
    ];

    // public function number()
    // {
    //     return $this->hasMany(Number::class);
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function campaignresult()
    {
        return $this->belongsTo(Campaignresult::class);
    }    
}
