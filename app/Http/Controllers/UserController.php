<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['page'] = 'Dashboard';
		$data['page_title'] = 'User';
        $data['users'] = User::OrderBy('id','ASC')->get();
        return view('dashboard.user.index',$data);
    }

    public function profile($id)
    {
        // return $id;
		$data['page'] = 'Dashboard';
		// $user_id = Auth::user()->id;
		// $data['user_info'] = User::where('id', $user_id)->get()->toArray();
		// $data['sess'] = $request->session()->all();
		$data['page_title'] = 'User';

        return view('dashboard.user.detail',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			return view('dashboard.user.add',[
				'page'=>'Users',
				'page_title' => 'Dasboard - users',
			]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			// dd("store");
			Validator::extend('without_spaces', function($attr, $value){
					return preg_match('/^\S*$/u', $value);
			});
			
			$validator = Validator::make($request->all(), [
				'name' => 'required',
				'username' => 'required|without_spaces',
				'email' => 'required|email|unique:users',
				[
					'username.without_spaces' => 'Whitespace not allowed.'
				]				
			]);

			if ($validator->fails()) {
				return redirect('users/')
										->withErrors($validator)
										->withInput();
		}			

			// $request->validate([
			// 		'name' => 'required',
			// 		'username' => 'required|without_spaces',
			// 		'email' => 'required|email|unique:users'
			// 	],

			// 	[
			// 			'username.without_spaces' => 'Whitespace not allowed.'
			// 	]);
			$validatedData['name'] = $request->name;
			$validatedData['username'] = $request->username;
			$validatedData['email'] = $request->email;
			$validatedData['password'] = bcrypt($request->password);
			$validatedData['role'] = $request->role;
      $validatedData['status'] = $request->status;
				// dd($validatedData);
			User::create($validatedData);

			$user = User::orderBy('created_at', 'desc')->first();
			$tenantId = '1000'.$user->id;
			$campaigntable = $tenantId.'_campaignresult';
			$numbertable = $tenantId.'_number';

			Schema::connection('mysql')->create($tenantId, function($table)
			{
				$table->increments('id');
				$table->string('campaign_id');
				$table->string('accoount_id');
				$table->string('name');
				$table->string('phone');
				$table->string('response');
				$table->string('call_start');
				$table->string('call_end');
				$table->string('call_status');
				$table->string('hangup_reason');
			});

			Schema::connection('mysql')->create($campaigntable, function($table)
			{
				$table->increments('id');    
				$table->integer('campaign_id')->unsigned();
				$table->dateTime('date_started');
				$table->dateTime('date_finished');
				$table->integer('total_call');
				$table->integer('success_call');
				$table->integer('failed_call');
				$table->integer('campaign_progress');       
				$table->timestamps();
			});			

			Schema::connection('mysql')->create($numbertable, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('campaign_id')->unsigned();
				$table->string('account_id')->nullable();
				$table->string('name');
				$table->string('phone');
				$table->string('action')->nullable();
				$table->string('response')->nullable();
				$table->integer('nominal')->nullable();
				$table->dateTime('bill_date')->nullable();
				$table->dateTime('due_date')->nullable();
				$table->dateTime('call_dial')->nullable();
				$table->dateTime('call_connect')->nullable();
				$table->dateTime('call_disconnect')->nullable();
				$table->time('call_duration')->nullable();
				$table->timestamps();
			});			

			return redirect('/users')->with('success','User created successfully.');

			// $input = $request->all();
			// $user = User::create($input);
			// return back()->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
			$user = User::find($id);
			return view('dashboard.user.detail',[
				'page'=>'Users',
				'page_title'=>'Dashboard - Users',
				'user' => $user,
			]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			return $id;
    }
}
