<?php

namespace App\Http\Controllers;

use App\User;
use App\Number;
use App\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$result = Campaign::where('msstatus_id',[1,3])->orderBy('id','DESC')->get();
			return view('dashboard.campaign.index',[
				'page'=>'Campaigns Active',
				'page_title' => 'Dasboard - Campaign',
				'campaign' => $result
			]);
    }			

		public function archive()
		{
			$result = Campaign::where('msstatus_id',2)->orderBy('id','DESC')->get();
			return view('dashboard.campaign.index',[
				'page'=>'Campaigns Archive',
				'page_title' => 'Dasboard - Campaign',
				'campaign' => $result
			]);			
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

		public function add(){
			return view('dashboard.campaign.add',[
				'page' => 'Campaign',
				'page_title' => 'Add',
			]);
		}

    public function create(Request $request)
    {
			return view('dashboard.campaign.add',[
				'page' => 'Campaign',
				'page_title' => 'Add',
			]);
		}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

			$validator = Validator::make($request->all(), [
				'campaign_name' => 'required|unique:campaigns|min:4|max:255',
			]);
      
			if ($validator->fails()) {
					return redirect('campaign/add')
											->withErrors($validator)
											->withInput();
			}

			$filename = $request->file_nomor;

			$i = 0;
			$data = Array();
			$file = json_decode($filename,true);
			
			if($file == null){
				return redirect('/campaigns')->with('warning','Failed to upload number, the file has to be .XLS!');				
			}

			foreach($file as $f){
				$i++;
				$data[$i] = $f;
			}
			$countdata = count($data);
			
			$validatedData['campaign_name'] = $request->campaign_name;
			$validatedData['user_id'] = auth()->user()->id;
			$validatedData['msstatus_id'] = 1;
			$validatedData['msresult_id'] = 1;
			$validatedData['qty'] = $i;
      		$validatedData['notes'] = '';

			Campaign::create($validatedData);

			$get = Campaign::orderBy('created_at', 'desc')->first();

			$campaign_id = $get->id;
			$insert_number = Array();

			foreach($data as $d){
					$number = [
						'account_id' => auth()->user()->id,
						'campaign_id' => $campaign_id,
						'action' => 0,
						'response' => null, 
						'name' => $d['name'],
						'phone' => $d['phone'],
						'bill_date' => $d['bill_date'],
						'due_date' => $d['due_date'],
						'nominal' => $d['nominal'],
						'call_dial' => null, 
						'call_connect' => null, 
						'call_disconnect' => null, 
						'call_duration' => null, 
					];

					$insert_number[] = $number;
				}

				$removefirst = array_shift($insert_number);	
				$insert_number = collect($insert_number);
				$chunks = $insert_number->chunk(200);
				foreach ($chunks as $chunk)
				{
					 DB::table('numbers')->insert($chunk->toArray());
				}				
			return redirect('/campaign')->with('success','New campaign has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\campaign  $campaign
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
			// return $id;
			$count_number = DB::table('numbers')->orderBy('status_id','DESC')->where('campaign_id',$id)->count();
			$campaign = Campaign::with('user')->with('campaignresult')->find($id);
			$numbers = Number::with('campaign')->where('campaign_id',$id)->get();
			return view('dashboard.campaign.detail',[
				'page'=>'Campaigns',
				'campaign' => $campaign,
				'total_number' => $count_number,
				'numbers' => $numbers,
			]);
		}	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
			// return $id;
			$button = $request->activebtn;
			// dd($button);
			if($button == 1){
				$params['msstatus_id'] = '2';
				$result = Campaign::where('id',$id)->update($params);
				if($result){
					$color = 'success';
					$message = 'Campaign has been deactivate!';
				} else {
					$color = 'success';
					$message = 'Campaign on ready or pause position';
				}
				return redirect('/campaign')->with($color,$message);
			} else {
				$params['msstatus_id'] = '1';
				Campaign::where('id',$id)->update($params);
				return redirect('/campaign')->with('success','Campaign has been activated!');
			}
    }

		public function runcampaign()
		{
			dd('Run Campaign');
		}
}
