<?php
namespace App\Http\Controllers;

use Session;
use App\User;
use DateTime;
use App\Campaign;
use App\Subscriber;
use Illuminate\View\View;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
//use Illuminate\Session\Store as SessionStore;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		return view('home');
	}

	public function getDashboard(Request $request)
	{
		$data['page'] = 'Dashboard';
		$user_id=Auth::user()->id;
		$data['user_info'] = User::where('id', $user_id)->get()->toArray();
		$data['sess'] = $request->session()->all();
		$data['page_title'] = 'Dashboard';
		$data['campaign'] = Campaign::where('msstatus_id',1)->orderBy('id','DESC')->get();
        // dd($data['campaign']);
		return view('dashboard.dashboard',$data);
	}	
	
}
