<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaignresult extends Model
{
    protected $fillable = [
        'date_started', 'date_finished', 'total_call', 'success_call', 'failed_call', 'campaign_progress'
    ];

    // public function campaign()
    // {
    //     return $this->belongsTo(Campaign::class);
    // }
}


