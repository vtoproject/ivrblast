@extends('layouts.master')

@section('content')
      <div class="row">
        <section class="col-lg-4">
          <div class="box box-solid">
            <div class="box-header">
              <div class="pull-right box-tools">
                <a href="#" type="button" class="btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="Minimize" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></a>
              </div>
              {{-- <i class="fa fa-volume-control-phone"></i> --}}
              <span class="h4">Live Call Resume</h3>
            </div>
            <div class="box-body">          
              <div class="shadow" id="halfdonut" style="background-color: #fff;"></div>
            </div>
          </div>
        </section>
        <section class="col-lg-8 connectedSortable">
          <div class="box box-solid">
            <div class="box-header">
              <div class="pull-right box-tools">
                <a href="#" type="button" class="btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Minimize" style="margin-right: 2px;"><i class="fa fa-minus"></i></a>
                <a href="{{ action('CampaignController@create') }}" type="button" class="btn-primary btn-sm" data-toggle="tooltip" title="Add New Campaign"><i class="fa fa-sticky-note-o"></i> New</a>
              </div>
              {{-- <i class="fa fa-volume-control-phone"></i> --}}
              <span class="h4"> Active Campaign</h3>
            </div>
            <div class="box-body">
              <div class="pt-5">
                <table id="datatbl" class="table table-condensed-xs table-hover">
                  <thead class="thead-dark">
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Start Date</th>
                      <th class="text-center">Name</th>
                      <th class="text-center">Total Data</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Created By</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>  
                  <tbody>
                    @foreach ($campaign as $c => $camp)
                    <tr>
                      <td class="text-center">{{ $c++ }}</td>
                      <td class="text-center">{{ date('d M Y', strtotime($camp->created_at)) }}</td>
                      <td class="text-center">{{ $camp->campaign_name }}</td>
                      <td class="text-center">{{ $camp->qty }}</td>
                      <td class="text-center"><strong><span id="percentage"></span></strong></td>
                      <td class="text-center">{{ $camp->user->name }}</td>
                      <td class="text-center" style="font-size: 18px;">
                        <a href="#"><i class="btn fa fa-play-circle" title="Play/Stop Campaign"></i></a> 
                        <a href="{{ action('CampaignController@show', $camp->id )}}"> <i class="fa fa-eye"></i></a>
                        {{-- <a href="{{ action('CampaignController@edit', $camp->id ) }}"><i class="btn fa fa-edit" title="Edit"> </i></a>  --}}
                        <a href="{{ action('CampaignController@destroy', $camp->id ) }}"> <i class="fa fa-trash" title="Deactivated"></i></a>
                      </td>         
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer no-border">
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <div id="sparkline-1"></div>
                  <div class="knob-label">Running</div>
                </div>
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <div id="sparkline-2"></div>
                  <div class="knob-label">Pause</div>
                </div>
                <div class="col-xs-4 text-center">
                  <div id="sparkline-3"></div>
                  <div class="knob-label">Ready</div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

@endsection

@push('scripts')
  <script src="{{ asset('js/apexcharts.js') }}"></script>
  <script src="{{ asset('js/apexscripts.js') }}"></script>
  <script src="{{ asset('js/playstop.js') }}"></script>
@endpush
