@extends('layouts.master')

@section('content')
<div class="row">
    <section class="col-lg-6">
      <div class="box">
        <div class="box">
          <div class="box-header">
            <div class="pull-right box-tools">
              <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-sticky-note-o" title="Add New User"></i></button>
            </div>
            <i class="fa fa-user"></i>
            <h3 class="box-title">
              Add User
            </h3>
          </div>
          <div class="collapse" id="collapseExample">
          <div class="box-body">
            <form class="form-horizontal" method="POST" action="{{ action('UserController@store') }}">
              {{ csrf_field() }}
  
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Name</label>
  
                  <div class="col-md-6">
                      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
  
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif  
                  </div>
              </div>
  
              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-md-4 control-label">Username</label>
  
                <div class="col-md-6">
                    <input id="username" type="text" class="form-control" name="username" value="{{ old('name') }}" required autofocus>
  
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif  
                </div>
            </div>
  
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="col-md-4 control-label">Email</label>
  
              <div class="col-md-6">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
  
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif  
              </div>
            </div>
  
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="col-md-4 control-label">password</label>
  
              <div class="col-md-6">
                  <input id="mypasswd" type="password" class="form-control" name="password" required autofocus>
                  <input type="checkbox" onclick="mypasswdfunct()"> Show Password 
  
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif  
              </div>
            </div>      
        
            <div class="form-group">
              <label for="role" class="col-md-4 control-label">Role</label>
  
              <div class="col-md-6">
                <select id="role" name="role" class="form-control">
                  <option value="superadmin">Super Admin</option>
                  <option value="admin">Admin</option>
                  <option value="supervisor">Supervisor</option>
                </select>
              </div>
            </div>        
  
              <div class="form-group">
                <label for="status" class="col-md-4 control-label">Status</label>
  
                <div class="col-md-6">
                  <select id="status" name="status" class="form-control">
                    <option value="active">Active</option>
                    <option value="deactive">Deactive</option>
                  </select>
                </div>
              </div>       
  
              <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <input type="hidden" id='file_nomor' name="file_nomor">
                      <button type="submit" class="btn btn-sm btn-primary">Create</button>
                      {{-- <a href="{{ url('/users') }}" class="btn btn-sm btn-warning">Cancel</a> --}}
                  </div>
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="col-lg-12">
        <div class="box">
          <div class="box-header">
            <div class="pull-right box-tools">
              {{-- <a href="{{ action('UserController@create') }}" type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Add new User"><i class="fa fa-sticky-note-o"></i></a> --}}
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
            </div>
            <i class="fa fa-users"></i>
            <h3 class="box-title">
              User List
            </h3>
          </div>
          <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table id="datatbl" class="table table-condensed-xs table-hover">
                <thead class="thead-dark">
                  <tr>
                    {{-- <th class="text-center">No</th> --}}
                    <th class="text-center">Action</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Role</th>
                  </tr>
                </thead>  
                <tbody>
                  @foreach ($users as $c => $user)
                  <tr>
                    {{-- <td class="text-center">{{ $c++ }}</td> --}}
                    <td class="text-center">
                      {{-- <a href="#"><i class="fa fa-play-circle" title="Play/Stop Campaign"></i></a> --}}
                      <a href="{{ action('UserController@show', $user->id) }}"><i class="fa fa-eye" title="Show"> </i></a>
                      {{-- <a href="{{ action('UserController@edit', $user->id) }}"><i class="fa fa-edit" title="Edit"> </i></a>  --}}
                      <a href="{{ action('UserController@destroy', $user->id) }}"><i class="fa fa-trash" title="Delete"></i></a>
                    </td>         
                    <td class="text-center">{{ $user->name }}</td>
                    <td class="text-center">{{ $user->username }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                    <td class="text-center">{{ $user->role }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>              
            </div>              
          </div>
        </div>
    </section>
  </div>
@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    var table = $('#datatbl').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );

function mypasswdfunct() {
  var x = document.getElementById("mypasswd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endpush