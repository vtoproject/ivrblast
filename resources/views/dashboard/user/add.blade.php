@extends('layouts.master')
@push('js')
<script lang="javascript" src="{{ asset('js/xlsx.full.min.js') }}"></script>
@endpush
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10">
      <div class="panel panel-default">
        <div class="panel-heading">New Campaign
          <a href="{{ url ('users') }}" class="pull-right" data-toggle="tooltip" title="Cancel add new"><i class="fa fa-close"></i> Cancel</a>
      </div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ action('UserController@store') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif  
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <label for="username" class="col-md-4 control-label">Username</label>

              <div class="col-md-6">
                  <input id="username" type="text" class="form-control" name="username" value="{{ old('name') }}" required autofocus>

                  @if ($errors->has('username'))
                      <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                      </span>
                  @endif  
              </div>
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif  
            </div>
          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">password</label>

            <div class="col-md-6">
                <input id="mypasswd" type="password" class="form-control" name="password" required autofocus>
                <input type="checkbox" onclick="mypasswdfunct()"> Show Password 

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif  
            </div>
          </div>      
      
            <div class="form-group">
              <label for="level" class="col-md-4 control-label">Role</label>

              <div class="col-md-6">
                <select id="level" name="level" class="form-control">
                  <option value="superadmin">Super Admin</option>
                  <option value="admin">Admin</option>
                  <option value="supervisor">Supervisor</option>
                  <option value="guest">Guest</option>
                </select>
              </div>
            </div>        

            <div class="form-group">
              <label for="status" class="col-md-4 control-label">Status</label>

              <div class="col-md-6">
                <select id="status" name="status" class="form-control">
                  <option value="Active">Active</option>
                  <option value="Deactive">Deactive</option>
                </select>
              </div>
            </div>       

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <input type="hidden" id='file_nomor' name="file_nomor">
                    <button type="submit" class="btn btn-sm btn-primary">Create</button>
                    {{-- <a href="{{ url('/users') }}" class="btn btn-sm btn-warning">Cancel</a> --}}
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  function mypasswdfunct() {
    var x = document.getElementById("mypasswd");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  </script>
@endpush