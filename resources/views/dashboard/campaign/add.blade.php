@extends('layouts.master')
@push('js')
<script lang="javascript" src="{{ asset('js/xlsx.full.min.js') }}"></script>
@endpush
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading">New Campaign
          <a href="{{ url ('campaign') }}" class="pull-right" data-toggle="tooltip" title="Cancel add new"><i class="fa fa-close"></i> Cancel</a>
      </div>
      <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ action('CampaignController@store') }}">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('campaign_name') ? ' has-error' : '' }}">
              <label for="campaign_name" class="col-md-4 control-label">Campaign Name</label>
              <a href="/download/sampleNumberImport.xlsx" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Sample Data"><i class="fa fa-download"></i></a>
              
              <div class="col-md-6">
                  <input id="campaign_name" type="text" class="form-control" name="campaign_name" value="{{ old('campaign_name') }}" required autofocus>

                  @if ($errors->has('campaign_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('campaign_name') }}</strong>
                      </span>
                  @endif  
              </div> 
          </div>
          <div class="form-group">
            <label for="name" class="col-md-4 control-label">Insert .xls file</label>
            <div class="col-md-6">
              <div class="input-group">
                <div class="wrapper">
                  <input class="form-control" type="file" id="excel_file" name="excel_file" value="{{ old('excel_file') }}" />
                </div>
              </div>
            </div>
          </div>            
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <input type="hidden" id='file_nomor' name="file_nomor">
                <button type="submit" class="btn btn-sm btn-primary">Create</button>
                <a href="{{ url('/campaign') }}" class="btn btn-sm btn-warning">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

    <div class="col-md-10 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading box-tools">
          Numbers Preview<br />
          {{-- <label class="small" for="">You can't change data in preview, please change in your Excel file</label> --}}
        </div>
        <div class="panel-body">           
          {{-- <div id="unique_data" class="mt-2 mb-4"></div> --}}
          {{-- <div id="excel_data" class="table mt-3"></div>     --}}

          {{-- <div class="mb-2"><span class="h5">Duplikat nomor</span></div>
          
          <div class="table table-responsive">
            <table class="table table-bordered table-striped">
              <thead></thead>
              <tbody id="unique_data"></tbody>
            </table>
          </div> --}}

          <div class="col-lg-12 table-responsive" style="max-height:500px; width: 100%;">
            <table class="table table-bordered table-striped table-condensed">
              <thead class="thead-dark">
                <th>No</th>
                <th>Acc Id</th>
                <th>Nama</th>
                <th>Phone</th>
                <th>Bill Date</th>
                <th>Due Date</th>
                <th>Nominal</th>
              </thead>
              <tbody id="excel_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  const excel_file = document.getElementById('excel_file');
  excel_file.addEventListener('change', (event) => {
      if(!['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'].includes(event.target.files[0].type))
      {
          document.getElementById('excel_data').innerHTML = '<div class="alert alert-danger">Only .xlsx or .xls file format are allowed</div>';
          excel_file.value = '';
          return false;
      }
      var reader = new FileReader();
      reader.readAsArrayBuffer(event.target.files[0]);
      reader.onload = function(event){
          var data = new Uint8Array(reader.result);
          var work_book = XLSX.read(data, {type:'array'});
          var sheet_name = work_book.SheetNames;
          var sheet_data = XLSX.utils.sheet_to_json(work_book.Sheets[sheet_name[0]], {header:1,raw:false,cellDates:true,dateNF:'YYYY/mm/dd h:mm:ss'});

          console.log("start");
          // console.log(sheet_data);

          var newarr = Array();

          if(sheet_data.length > 0){
            for(var rowarr = 0; rowarr < sheet_data.length; rowarr++)
              {
                for(cellarr=1;cellarr < sheet_data[rowarr].length;cellarr++){
                  newarr[rowarr] = sheet_data[rowarr][2].replace(/^0+/, '');
                }
              }
          }

          // console.log(newarr);

          const yourArray = newarr

          let duplicates = []

          const tempArray = [...yourArray].sort()

          for (let i = 0; i < tempArray.length; i++) {
            if (tempArray[i + 1] === tempArray[i]) {
              duplicates.push(tempArray[i])
            }
          }

          function uniqueArray2(arr) {
              var a = [];
              for (var i=0, l=arr.length; i<l; i++)
                  if (a.indexOf(arr[i][2]) === -1 && arr[i][2] !== '')
                      a.push(arr[i]);
              return a;
          }
          sort_data = uniqueArray2(sheet_data);

          var arrayWithDuplicates = sheet_data

          function removeDuplicates(originalArray, prop) {
              var newArray = [];
              var lookupObject  = {};

              for(var i in originalArray) {
                  lookupObject[originalArray[i][prop]] = originalArray[i];
              }

              for(i in lookupObject) {
                newArray.push(lookupObject[i]);
              }
                return newArray;
          }

          var uniqueArray = removeDuplicates(arrayWithDuplicates, "2");
          sheet_data_clean = uniqueArray;

          // console.log(sheet_data_clean);

          var jsonObj = {};
          for (var i = 0 ; i < uniqueArray.length; i++) {
              jsonObj[(i+1)] = uniqueArray[i];
          }

          var offset = -8;
          var newdate = new Date();
          
          console.log(newdate);

          format = function date2str(x, y) {
              var z = {
                  M: x.getMonth() + 1,
                  d: x.getDate(),
                  h: x.getHours(),
                  m: x.getMinutes(),
                  s: x.getSeconds()
              };
              y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
                  return ((v.length > 1 ? "0" : "") + z[v.slice(-1)]).slice(-2)
              });

              return y.replace(/(y+)/g, function(v) {
                  return x.getFullYear().toString().slice(-v.length)
              });
          }          
          
          var datenow = format(newdate, 'yyyy/mm/dd h:m:s')

          var data = uniqueArray;
          var json = data.map(function (value, key) {
              return {
                  'account_id': {{ auth()->user()->id }},
                  'account_id': value[0],
                  'name': value[1],
                  'phone': parseInt(value[2]),
                  'bill_date': value[3],
                  'due_date': value[4],
                  'nominal': value[5],
              }
          });

          // console.log(json)
          console.log(JSON.stringify(json));

          $("#file_nomor").val(JSON.stringify(json));

        t_unique = "";
          var table_output = '<tr><td colspan="7"></td></tr>';
          for(var row = 1; row < sheet_data_clean.length; row++)
          {
            $num = parseInt(sheet_data_clean[row][2]);
            i++;
            table_output += '<tr>';
            table_output += '<td>'+row+'<td>';
              table_output += '<td>'+sheet_data_clean[row][1]+'</td>';
              table_output += '<td>'+$num+'</td>';
              table_output += '<td>'+sheet_data_clean[row][3]+'</td>';
              table_output += '<td>'+sheet_data_clean[row][4]+'</td>';
              table_output += '<td>'+sheet_data_clean[row][5]+'</td>';
            table_output += '</tr>';
          }
          document.getElementById('excel_data').innerHTML = table_output;
          excel_file.value = '';
      }
  });
</script>
@endpush