@extends('layouts.master')

@section('content')
<div class="row">
    <section class="col-lg-12 connectedSortable">
      <div class="box">
        <div class="box">
          <div class="box-header">
            <div class="pull-right box-tools">
              <a href="{{ action('CampaignController@create') }}" type="button" class="btn btn-primary btn-sm daterange" data-toggle="tooltip" title="Add new Campaign"><i class="fa fa-sticky-note-o"></i></a>
              <a href="{{ url('campaign') }}" type="button" class="btn btn-primary btn-sm daterange" data-toggle="tooltip" title="Active Campaigns"><i class="fa fa-bell"></i></a>
              <a href="{{ url('campaign/archive') }}" type="button" class="btn btn-primary btn-sm daterange" data-toggle="tooltip" title="Archive Campaigns"><i class="fa fa-archive"></i></a>
              <button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
            </div>

            {{-- <i class="fa fa-volume-control-phone"></i> --}}
            <h3 class="box-title">
              {{ $page }}
            </h3>
          </div>
          <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
              <table id="datatbl" class="table table-hover">
                <thead class="thead-dark">
                  <tr>
                    {{-- <th class="text-center">No</th> --}}
                    <th class="text-center">Action</th>
                    <th class="text-center">Name(s)</th>
                    <th class="text-center">Date/Time</th>
                    <th class="text-center">Total Data</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Created By</th>
                  </tr>
                </thead>  
                <tbody>
                  @foreach ($campaign as $c => $camp)
                  <tr>
                    {{-- <td class="text-center">{{ $c++ }}</td> --}}
                    <td class="text-center">
                      {{-- <a href="#"><i class="fa fa-play-circle" title="Play/Stop Campaign"></i></a>  --}}

                      {{-- <a href="{{ action('CampaignController@destroy', $camp->id ) }}"><i class="fa fa-trash" title="Delete"></i></a>    --}}
                      <form action="{{ route('campaign.destroy' , $camp->id)}}" method="POST" class="d-inline">
                        <input name="_method" type="hidden" value="DELETE">
                        {{ csrf_field() }}
                          <a href="{{ action('CampaignController@show', $camp->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye" title="Show"></i></a>
                          <a href="{{ action('CampaignController@edit', $camp->id ) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit" title="Edit"></i></a>   
                          <button type="submit" class="btn btn-xs btn-warning" name="activebtn" value="{{ $camp->msstatus_id }}" title="Deactivated campaign">
                            <i class="fa fa-times"></i>                                
                          </button>
                      </form>
                    </td>         
                    <td class="text-center">{{ $camp->campaign_name }}</td>
                    <td class="text-center">{{ date('d M Y', strtotime($camp->created_at)) }}</td>
                    <td class="text-center">{{ $camp->qty }}</td>
                    <td class="text-center"><strong></strong></td>
                    <td class="text-center">{{ $camp->user->name }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>              
            </div>              
          </div>
        </div>
      </div>
    </section>
    {{-- <section class="col-lg-4 connectedSortable" id="detail">
      <div class="box">
        <div class="box">
          <div class="box-header">
            <i class="fa fa-info-circle"></i>

            <h3 class="box-title">
              Detail
            </h3>
          </div>
          <div class="box-body">
            <p id="detail">This is a paragraph.</p>
            <button id="btndetail">Toggle between hide() and show()</button>
          </div>
        </div>
      </div>
    </section>     --}}
  </div>
@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    var table = $('#datatbl').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
</script>
@endpush