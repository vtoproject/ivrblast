@extends('layouts.master')

@section('content')
<div class="row">
   <section class="col-lg-6">
    <div class="box">
      <div class="box">
        <div class="box-header">
          <div class="pull-right box-tools">
          </div>
          {{-- <i class="fa fa-file"></i> --}}
          <h3 class="box-title"> Info</h3>
        </div>
        <div class="box-body">
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Campaign Name</label>
            <div class="col-md-6">{{ $campaign->campaign_name }}</div>
          </div>     
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Total Number Call</label>
            <div class="col-md-6">{{ $total_number }}</div>
          </div> 
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Status</label>
            <div class="col-md-6">{{ $campaign->msstatus_id }}</div>
          </div>    
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Created By</label>
            <div class="col-md-6">{{ $campaign->user->name }}</div>
          </div>                                              
        </div>
      </div>
    </div>
  </section>
  <section class="col-lg-6">
    <div class="box">
      <div class="box">
        <div class="box-header">
          {{-- <i class="fa fa-file"></i> --}}
          <h3 class="box-title"> Result Campaign</h3>
        </div>
        <div class="box-body">
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Date Started</label>
            <div class="col-md-6"></div>
          </div>     
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Date Finish</label>
            <div class="col-md-6"></div>
          </div> 
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Total Call</label>
            <div class="col-md-6"></div>
          </div>    
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Success Call</label>
            <div class="col-md-6"></div>
          </div>  
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Failed Call</label>
            <div class="col-md-6"></div>
          </div>                                                       
          <div class="col-lg-12">
            <label for="campaign_name" class="col-md-4 control-label">Campaign Progress %</label>
            <div class="col-md-6"></div>
          </div>                                              
        </div>
      </div>
    </div>
  </section> 
  <section class="col-lg-12 connectedSortable">
    <div class="box">
      <div class="box-header">
        {{-- <i class="fa fa-file"></i> --}}
        <h3 class="box-title"> Detail Call</h3>
      </div>
      <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 750px; width: 100%;">
          <table id="datatbl" class="table table-hover">
            {{-- <table id="datatbl" class="display"> --}}
            <thead class="thead-dark">
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">Acc ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Phone</th>
                <th class="text-center">Bill Date</th>
                <th class="text-center">Due Date</th>
                <th class="text-center">Nominal</th>
                <th class="text-center">Call Date</th>
                <th class="text-center">Call Response</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>  
            <tbody>
              @foreach($numbers as $n => $numb)
              <tr>
                <td class="text-center">{{ $n++ }}</td>
                <td class="text-center">{{ $numb->account_id }}</td>
                <td class="text-center">{{ $numb->name }}</td>
                <td class="text-center">{{ $numb->phone }}</td>
                <td class="text-center">{{ $numb->bill_date }}</td>
                <td class="text-center">{{ $numb->due_date }}</td>
                <td class="text-center">{{ $numb->nominal }}</td>
                <td class="text-center">{{ $numb->call_date }}</td>
                <td class="text-center">{{ $numb->response }}</td>
                <td class="text-center"><a href=""><i class="fa fa-eye"></i></a></td>
              </tr>
              @endforeach
            </tbody>
          </table>              
        </div>   
      </div>
    </div>
  </section> 
</div>
@endsection

@push('scripts')
<script>
  $(document).ready(function() {
    var table = $('#datatbl').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
</script>
@endpush