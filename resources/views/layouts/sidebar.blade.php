 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{ url ('dashboard') }}">
            <i class="fa fa-th"></i> <span>Dashboard</span>
          </a>
        </li>
		    <li>
          <a href="{{ url ('campaign') }}">
            <i class="fa fa-phone-square"></i> <span>Campaign</span>
          </a>
        </li>
		    <li>
          <a href="{{ url ('report') }}">
            <i class="fa fa-book"></i> <span>Report</span>
          </a>
        </li>        
        @if(Auth::user()->role == 'superuser')
          <li>
            <a href="{{ url ('users') }}">
              <i class="fa fa-users"></i> <span>Users</span>
            </a>
          </li>  
        @endif
        <li>
          <a href="{{ action('UserController@show', Auth::user()->id )}}">
            <i class="fa fa-user"></i> <span>Profile</span>
          </a>
        </li>           
        <li>
          <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i><span>Logout</span></a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>        

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>