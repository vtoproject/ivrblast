<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ $page_title or "Robodal Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
   

  <link rel="stylesheet" href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/bower_components/font-awesome/css/font-awesome.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/bower_components/Ionicons/css/ionicons.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/dist/css/AdminLTE.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/dist/css/skins/_all-skins.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/bower_components/select2/dist/css/select2.min.css")}}">
  <link rel="stylesheet" href="{{ asset("/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"> --}}
  <link rel="stylesheet" href="{{ asset('/css/rowReorder.dataTables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/responsive.dataTables.min.css') }}">

  @stack('css')

  </head>
  <body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
    <div class="wrapper">

      @include('layouts.header')

      @include('layouts.sidebar')

      <div class="content-wrapper">
        <section class="content">  
          @if(session()->has('success'))
          <div class="alert alert-success col-lg-8" role="alert">
            {{ session('success') }}
          </div>
          @endif        
          @yield('content')
        </section>
      </div>

      @include('layouts.footer')

    </div>
  
    <script src="{{ asset ('/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset ('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset ('/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

    @stack('apexjs')
    @stack('scripts')
    @stack('js')

  </body>
</html>