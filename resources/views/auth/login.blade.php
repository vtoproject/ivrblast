@extends('layouts.app')

@section('content')
<div class="container">  
	<div class="col-md-10 col-md-offset-1 main" >
		<div class="col-md-6 left-side" >
		<h3>ROBODAL IVR</h3>
		<p>Pt. Dalnet System</p>
		<br>
		</div>
		
		<div class="col-md-6 right-side">
			<h3>Login</h3>
			<div class="form">
				<form class="form-horizontal" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}
					<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
					<label for="username">Your username</label>
							<input type="text" id="username" class="form-control input-lg" name="username" required>
					</div>
					<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password">Your password</label>
							<input type="password" id="password" class="form-control input-lg" name="password" required>
					</div>
					<div class="text-xs-center">
							<button class="btn btn-deep-purple">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@push('css')
	<link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endpush

@push('scripts')
	{{-- <script src="{{ asset('js/login.js') }}"></script> --}}
@endpush